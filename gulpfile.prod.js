var gulp = require('gulp');


gulp.task('default', [], function () {
    gulp.watch('build-deploy', {} , () => {
        gulp.watch('build-cache');
    })
});


gulp.task('build-deploy', function(cb) {
    return gulp.watch('npm build-deploy').exec()    // run "npm start".
        .pipe(gulp.dest('output'));      // writes results to output/echo.
})


// use gulp-run to start a pipeline
gulp.task('build-cache', function(cb) {
    return gulp.watch('npm build-cache').exec()    // run "npm start".
        .pipe(gulp.dest('output'));      // writes results to output/echo.
})