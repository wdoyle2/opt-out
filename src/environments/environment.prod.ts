// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
    production: true,
    apiKey: 'p2CO1PZvZn6nmSGWnIDkz9tKac5zDDBy7KsjjVWt',
    base_url: 'https://api.txtsync.com/',
    endpoints: {
        authentication: 'auth/',
        contacts: 'contacts/',
        numbers: 'numbers/',
        sms: 'sms/',
        tags: 'tags/',
        library: 'library/',
        billing: 'billing/',
        system: 'system/',
        sync: 'sync/',
        campaigns: 'campaigns/'
    },
    pusher: {
        key: 'd581f3581b26d4844633',
        cluster: 'eu',
    }
};
