// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    apiKey: 'p2CO1PZvZn6nmSGWnIDkz9tKac5zDDBy7KsjjVWt',
    base_url: 'https://api.txtsync.com/',
    endpoints: {
        authentication: 'auth/',
        contacts: 'contacts/',
        numbers: 'numbers/',
        sms: 'sms/',
        tags: 'tags/',
        library: 'library/',
        billing: 'billing/',
        system: 'system/',
        sync: 'sync/',
        campaigns: 'campaigns/'
    },
    pusher: {
        key: 'd581f3581b26d4844633',
        cluster: 'eu',
    }
    /*
    apiKey: 'dcNjCuQXwG8DnkzzS6SoX5DvVlYOhbes7ZnNNQsV',
    base_url: 'https://dev.api.txtsync.com/',
    endpoints: {
        authentication: 'auth/',
        contacts: 'contacts/',
        numbers: 'numbers/',
        sms: 'sms/',
        tags: 'tags/',
        library: 'library/',
        billing: 'billing/',
        system: 'system/',
        sync: 'sync/',
        campaigns: 'campaigns/'
    },
    pusher: {
        key: 'e1ab7a3baed3e61ef594',
        cluster: 'eu',
    }*/
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
