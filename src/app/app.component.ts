import {Component, OnInit} from '@angular/core';
import {ApiService} from './api.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'optout';
  formID: any = '';

  constructor(public ApiSerivce: ApiService, public translate: TranslateService) {
    translate.addLangs(['en', 'fr', 'de', 'en-MX', 'es-MX', 'pt-BR']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr|de|en-MX|es-MX|pt-BR/) ? browserLang : 'en');
  }

  ngOnInit(): void {
    console.log('Testing');
  }
}
