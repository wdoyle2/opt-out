import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {TwofactorComponent} from './twofactor.component';
import {LoadingScreenComponent} from './loading-screen.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Ng2TelInputModule} from 'ng2-tel-input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MoveNextByMaxLengthDirective} from './moveup.directive';
import {ProfilePageComponent} from './profilePage.component';
import {AvatarModule} from 'ngx-avatar';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

const appRoutes: Routes = [
    {path: '2factor', component: TwofactorComponent},
    {path: 'profile', component: ProfilePageComponent},
    {path: ':id', component: LoadingScreenComponent},
    {path: '**', component: LoadingScreenComponent}
];

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent,
        LoadingScreenComponent,
        TwofactorComponent,
        ProfilePageComponent,
        MoveNextByMaxLengthDirective,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AvatarModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ReactiveFormsModule,
        Ng2TelInputModule,
        HttpClientModule,
        RouterModule.forRoot(
            appRoutes
        )
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
