import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService {


    constructor(public http: HttpClient) {
    }


    async getform(formID): Promise<any> {
        return await this.http.get(environment.base_url + 'system/customers/' + formID, {
            headers: {
                'x-api-key': environment.apiKey,
            }
        }).toPromise();
    }

    async getConsertAuth(formID, mobileNumber): Promise<any> {
        return await this.http.get(environment.base_url + 'consent/web/auth/' + formID, {
            params: {
                mobilenumber: mobileNumber.replace("+", "%2B"),
            },
            headers: {
                'x-api-key': environment.apiKey,
            }
        }).toPromise();
    }


    async getMe(key, code): Promise<any> {
        return await this.http.post(environment.base_url + 'consent/web/auth/', {
            key: key,
            code: code
        }, {
            params: {},
            headers: {
                'content-type': 'application/json',
                'x-api-key': environment.apiKey,
            }
        }).toPromise();
    }


    async updateMe(accessToken, model): Promise<any> {
        return await this.http.put(environment.base_url + 'consent/me/subscribe', model, {
            params: {},
            headers: {
                'Authorization': 'Bearer ' + accessToken,
                'content-type': 'application/json',
                'x-api-key': environment.apiKey,
            }
        }).toPromise();
    }
}
