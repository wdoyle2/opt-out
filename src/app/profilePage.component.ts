import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiService} from './api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {default as swal} from 'sweetalert2';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

declare const chrome: any;

@Component({
  selector: 'app-profile-page',
  templateUrl: './profilePage.component.html'
})

export class ProfilePageComponent implements OnInit {

  public CustomerDetails: any = false;
  public userDetails: any = false;
  public storedDetails: any = false;
  public thankyou: any = '';
  public error: any = '';

  constructor(public apiService: ApiService, public router: Router, public translate: TranslateService) {
    console.log('Loading details');
  }

  async loadDetailsFromStorage() {
    const details = await localStorage.getItem('CustomerDetails');
    if (details !== null && details !== '') {
      const CustomerDetails = JSON.parse(details);
      if (CustomerDetails.WebOptOutSettings.Theme) {
        const head = document.getElementsByTagName('head')[0];
        const currentCSS = document.getElementById('ExternalCSS');
        if (currentCSS) {
          head.removeChild(currentCSS);
        }
        const style = document.createElement('style');
        style.type = 'text/css';
        style.id = 'ExternalCSS';
        style.appendChild(document.createTextNode(CustomerDetails.WebOptOutSettings.Theme));
        head.appendChild(style);
      }
      this.CustomerDetails = CustomerDetails;

      console.error(this.CustomerDetails);
    }


    const userDetails = await localStorage.getItem('UserDetails');
    if (userDetails !== null && userDetails !== '') {
      this.userDetails = JSON.parse(userDetails);
      this.storedDetails = JSON.parse(userDetails);
      console.error(this.userDetails);
    }

    this.translate.get('thankYou', { value: this.userDetails.Contact.FirstName}).subscribe((res: string) => {
      this.thankyou = res;
    });
    this.translate.get('error').subscribe((res: string) => {
      this.error = res;
    });
  }


  ngOnInit() {
    this.loadDetailsFromStorage();
  }

  showError(message) {
    return swal.fire({
      title: message,
      type: 'error'
    });
  }

  async saveProfile() {
    const profile: any = {
      Tags: []
    };
    if (this.userDetails.Contact.AllowSMS !== this.storedDetails.Contact.AllowSMS) {
      profile.AllowSMS = this.userDetails.Contact.AllowSMS;
    }
    if (this.userDetails.Contact.Tags.length > 0) {
      for (const tagIndex in this.storedDetails.Contact.Tags) {
        if (tagIndex) {
          const StoredTag = this.storedDetails.Contact.Tags[tagIndex];
          const UpdatedTag = this.userDetails.Contact.Tags[tagIndex];
          if (StoredTag.Subscribed !== UpdatedTag.Subscribed) {
            profile.Tags.push({
              TagID: StoredTag.TagID,
              Subscribed: UpdatedTag.Subscribed
            });
          }
        }
      }
    }
    console.log(profile);


    try {

      if (typeof profile.AllowSMS !== 'undefined' || profile.Tags.length > 0) {
        const res = await this.apiService.updateMe(this.userDetails.AccessToken, profile);
        swal.fire({
          title: this.thankyou,
          type: 'success'
        });
        this.router.navigateByUrl(this.CustomerDetails.ExternalReference);

        console.log(res);
      } else {
        swal.fire({
          title: this.thankyou,
          type: 'success'
        });
      }
    } catch (err) {
      swal.fire({
        title: this.error,
        type: 'error'
      });
    }

  }


}
