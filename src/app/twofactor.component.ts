import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiService} from './api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {TranslateService} from '@ngx-translate/core';

declare const chrome: any;
declare const intlTelInputUtils: any;

@Component({
  selector: 'app-two-factor',
  templateUrl: './twofactor.component.html'
})
export class TwofactorComponent implements OnInit {

  public CustomerDetails: any = false;
  public phoneForm: any = false;
  public phoneFormModel: any = false;
  public codeSent: any = false;
  public ContactTelephone: any = '';
  public codeForm: any = false;

  public code1: any = '';
  public code2: any = '';
  public code3: any = '';
  public code4: any = '';

  public loadingText: any = '';

  intlInput: any = false;

  numberType: any = {
    'FIXED_LINE': 0,
    'MOBILE': 1,
    'FIXED_LINE_OR_MOBILE': 2,
    'TOLL_FREE': 3,
    'PREMIUM_RATE': 4,
    'SHARED_COST': 5,
    'VOIP': 6,
    'PERSONAL_NUMBER': 7,
    'PAGER': 8,
    'UAN': 9,
    'VOICEMAIL': 10,
    'UNKNOWN': -1
  };

  TypedNumber: any = '';
  PhoneCountry: any = false;


  modalOptions: any = {
    initialCountry: 'auto',
    geoIpLookup: function (success, failure) {
      $.get('https://ipinfo.io?token=06ffc0184840ee', () => {
      }, 'jsonp').always(function (resp) {
        const countryCode = (resp && resp.country) ? resp.country : '';
        success(countryCode);
      });
    },
  };


  @ViewChild('code1Element') code1Element: ElementRef;
  @ViewChild('code2Element') code2Element: ElementRef;
  @ViewChild('code3Element') code3Element: ElementRef;
  @ViewChild('code4Element') code4Element: ElementRef;

  constructor(public apiService: ApiService, public router: Router, public translate: TranslateService) {
    this.phoneForm = new FormGroup({
      phone: new FormControl(undefined, [Validators.required])
    });

    this.codeForm = new FormGroup({
      code1: new FormControl(undefined, [Validators.required, Validators.maxLength(1)]),
      code2: new FormControl(undefined, [Validators.required, Validators.maxLength(1)]),
      code3: new FormControl(undefined, [Validators.required, Validators.maxLength(1)]),
      code4: new FormControl(undefined, [Validators.required, Validators.maxLength(1)])
    });

    this.translate.get('loading').subscribe((res: string) => {
      this.loadingText = res;
    });

  }

  async nextInput() {
    console.log(this);
    if (this.code1 === '') {
      this.code1Element.nativeElement.focus();
    } else if (this.code2 === '') {
      this.code2Element.nativeElement.focus();
    } else if (this.code3 === '') {
      this.code3Element.nativeElement.focus();
    } else if (this.code4 === '') {
      this.code4Element.nativeElement.focus();
    } else {
      this.checkCode();
    }
  }

  async loadDetailsFromStorage() {
    const details = await localStorage.getItem('CustomerDetails');
    if (details !== null && details !== '') {
      const CustomerDetails = JSON.parse(details);
      if (CustomerDetails.WebOptOutSettings.Theme) {
        const head = document.getElementsByTagName('head')[0];
        const currentCSS = document.getElementById('ExternalCSS');
        if (currentCSS) {
          head.removeChild(currentCSS);
        }
        const style = document.createElement('style');
        style.type = 'text/css';
        style.id = 'ExternalCSS';
        style.appendChild(document.createTextNode(CustomerDetails.WebOptOutSettings.Theme));
        head.appendChild(style);
      }
      this.CustomerDetails = CustomerDetails;
      console.error(this.CustomerDetails);
    }
  }


  hasError(e) {
    console.log(e);
  }

  getNumber(e) {
    console.log(e);
    this.TypedNumber = this.intlInput.getNumber();
    this.PhoneCountry = this.intlInput.getSelectedCountryData();
    if (this.intlInput && this.intlInput.isValidNumber()) {
      console.log('TEST');
      this.ContactTelephone = this.TypedNumber;
    } else {
      this.ContactTelephone = false;
      console.log('failed)');
    }

  }

  telInputObject(intlInput) {
    console.log(intlInput);
    this.intlInput = intlInput;
  }

  onCountryChange(e) {
    this.PhoneCountry = e;
  }

  ngOnInit() {
    this.loadDetailsFromStorage();
  }


  async checkDetails() {
    try {
      console.log(this.ContactTelephone);
      if (this.ContactTelephone) {
        const phoneNumber = this.intlInput.getNumber().replace(' ', '');

        Swal.fire({
          title: this.loadingText,
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          onOpen: () => {
            Swal.showLoading();
          }
        });
        const consentAuth = await this.apiService.getConsertAuth(this.CustomerDetails.ExternalReference, phoneNumber);
        console.log(consentAuth);
        if (consentAuth && consentAuth.key) {
          localStorage.setItem('key', consentAuth.key);
          localStorage.setItem('LocalNumber', this.intlInput.getNumber(intlTelInputUtils.numberFormat.NATIONAL));
          localStorage.setItem('E164Number', phoneNumber);
          this.codeSent = {
            key: consentAuth.key,
            localNumber: this.intlInput.getNumber(intlTelInputUtils.numberFormat.NATIONAL),
            e164: phoneNumber
          };
          Swal.close();
          setTimeout(() => {
            this.nextInput();
          }, 200);
        }
      }

    } catch (err) {
      if (err.error.Message) {
        Swal.fire('An error occurred', err.error.Message, 'error');
      } else {
        console.log(err);
        Swal.close();
      }
    }
  }

  async checkCode() {
    try {


      if (this.code1 === '') {
        this.code1Element.nativeElement.focus();
      } else if (this.code2 === '') {
        this.code2Element.nativeElement.focus();
      } else if (this.code3 === '') {
        this.code3Element.nativeElement.focus();
      } else if (this.code4 === '') {
        this.code4Element.nativeElement.focus();
      } else {

        Swal.fire({
          title: this.loadingText,
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          onOpen: () => {
            Swal.showLoading();
          }
        });
        const code = this.code1 + this.code2 + this.code3 + this.code4;
        const response = await this.apiService.getMe(this.codeSent.key, code);
        if (response) {
          localStorage.setItem('UserDetails', JSON.stringify(response));
          Swal.close();
          this.router.navigateByUrl('profile');
        } else {
          console.error('No response');
        }


      }
    } catch (err) {
      Swal.close();
      console.error(err);
    }

  }


}
