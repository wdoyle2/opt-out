import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {ApiService} from './api.service';
import {ActivatedRoute, Router} from '@angular/router';

declare const chrome: any;

@Component({
    selector: 'app-loading-screen',
    templateUrl: './loadingScreen.component.html'
})

export class LoadingScreenComponent implements OnInit {

    public formID: any = false;
    public formDetails: any = false;

    constructor(public apiService: ApiService, private route: ActivatedRoute, public router: Router) {

    }


    async loadForm() {
        try {

            const formResponse = await this.apiService.getform(this.formID);
            if (formResponse) {

                if (formResponse.WebOptOutSettings.Theme) {
                    const head = document.getElementsByTagName('head')[0];
                    const currentCSS = document.getElementById('ExternalCSS');
                    if (currentCSS) {
                        head.removeChild(currentCSS);
                    }
                    const style = document.createElement('style');
                    style.type = 'text/css';
                    style.id = 'ExternalCSS';
                    style.appendChild(document.createTextNode(formResponse.WebOptOutSettings.Theme));
                    head.appendChild(style);
                }


                localStorage.setItem('CustomerDetails', JSON.stringify(formResponse));
                this.router.navigateByUrl('2factor');
            } else {
                this.showError('You are currently unable to opt out of messaging.');
                this.formDetails = true;
            }
        } catch (err) {
            this.showError('You are currently unable to opt out of messaging.');
            this.formDetails = true;
        }
    }

    async checkStoredID() {
        const Details: any = await localStorage.getItem('CustomerDetails');
        if (typeof Details !== 'undefined' && Details !== null && Details !== false) {
            this.formID = Details.ExternalReference;
            this.loadForm();
        } else {
            this.showError('You are currently unable to opt out of messaging.');
            this.formDetails = true;
        }
    }

    ngOnInit() {
        this.formID = this.route.snapshot.paramMap.get('id');
        if (this.formID == null || this.formID == '') {
            return this.checkStoredID();
        }
        this.loadForm();
    }


    showError(message) {
        // return Swal.fire({
        //     title: message,
        //     type: 'error'
        // });
    }


}
